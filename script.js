let currentPlayer = 'X';  //sets player 1
let nextPlayer = 'O';  //sets player 2
let playerXSelections = [];  //builds placeholder array of moves by player 1
let playerOSelections = [];  //builds placeholder array of moves by player 2

const winningCombinations = [  //object array of various win conditions
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];

const handleClick = function(event) {  //logs id of cell that was clicked
    const cell = event.target
    console.log(cell.id);
}
const cells = document.querySelectorAll('td');  //gets an array of all the cells
for (let i = 0; i < cells.length; i++) {  //iterates through the cells to add an 'event listener'
    cells[i].addEventListener('click', handleClick);
}